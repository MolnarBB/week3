#include <iostream>
#include <vector>
#include <queue>
#include<set>
#include <stack>
#include <array>
#include <list>
#include <map>

void printQueue(std::queue<int> queue)
{
	for (unsigned index = 0; index < queue.size(); ++index)
	{
		std::cout << queue.front() << " ";
		queue.pop();
	}
}
void print(std::stack<int> myStack)
{
	while (!myStack.empty())
	{
		std::cout << myStack.top() << ' ';
		myStack.pop();
	}
}

template <typename Iterator>
void print(Iterator first, Iterator last)
{
	for (auto it = first; it != last; ++it)
		std::cout << *it << ' ';
}

void vectorIterators()
{
	constexpr int nrElements = 5;
	std::vector<int> vec;
	for (auto it = 1; it <= nrElements; it++)
		vec.push_back(it);

	for (auto it = vec.cbegin(); it != vec.cend(); ++it)
		std::cout << *it << " ";
	std::cout << std::endl;

	for (auto it = vec.rbegin(); it != vec.rend(); ++it)
		std::cout << *it << " ";
	std::cout << std::endl;

	// Create a vector of ints and push elements 1->5 inside
	// Iterate the vector using begin - end
	// What about constant iteration
	// Can we iterate reverse?
	// Constant reverse
}

void vectorCapacity()
{
	// Create a vector of ints and push elements 1->5 inside
	std::vector<int> myVec;
	myVec.push_back(1);
	myVec.push_back(2);
	myVec.push_back(3);
	myVec.push_back(4);
	myVec.push_back(5);

	std::cout << "Size : " << myVec.size();
	std::cout << "\nMax_Size : " << myVec.capacity();

	// resizes the vector size to 4
	myVec.resize(4);
	// prints the vector size after resize()
	std::cout << "\nSize : " << myVec.size();

	// checks if the vector is empty or not
	if (!myVec.empty())
	{
		std::cout << "\nVector elements are: ";
		for (auto it : myVec)
		{
			std::cout << it << " ";
		}
		std::cout << "\n";
	}
	else
	{
		std::cout << " \nVectorul este go\n";
	}
}

void vectorModifiers()
{
	// Assign vector
	// fill the array with 10 five times
	std::vector<int> myVector(5, 10);

	std::cout << "The vector elements are: ";
	print(std::cbegin(myVector), std::cend(myVector));
	// inserts 15 to the last position
	myVector.emplace_back(15);
	std::cout << "\nThe last element is: " << myVector.back();

	// removes last element
	myVector.pop_back();
	// prints the vector
	std::cout << "\nThe vector elements are: ";
	print(std::cbegin(myVector), std::cend(myVector));
	// inserts 5 at the beginning
	myVector.insert(std::begin(myVector), 5);
	std::cout << "\nThe first element is: " << myVector.front();
	// removes the first element
	myVector.erase(std::begin(myVector));
	std::cout << "\nThe first element is: " << myVector.front();

	// Inserts 20 at the end
	myVector.emplace_back(20);
	std::cout << "\nThe last element is: " << myVector.back();

	// erases the vector
	myVector.clear();

	std::cout << "\nVector size after erase(): " << myVector.size();
}


void vectorAccess()
{
	// Create a vector of ints and push some elements 

	std::vector<int> vec;
	int nrElements = 10;
	for (auto it = 1; it <= nrElements; it++)
		vec.push_back(it);

	std::cout << "\nReference operator [] = ";
	for (auto it = 0; it < nrElements; it++)
		std::cout << vec[it] << " ";
	
	std::cout << "\nat = ";
	for (auto it = 0; it < nrElements; it++)
		std::cout << vec.at(it) << " ";

	std::cout << "\nGet the first element = ";
	std::cout << *vec.cbegin();

	std::cout << "\nGet the last element = ";
	std::cout << *(vec.cend() - 1);

	// pointer to the first element

	std::cout << "\nThe first element is " << *vec.cbegin();
}


void howToList()
{
	// Create and fill a list of ints
	std::list<int> myList;
	myList.resize(10);
	std::fill(std::begin(myList), std::end(myList), 15);
	std::cout << "\nList is : ";
	print(std::begin(myList), std::end(myList));

	std::cout << "\nGet the first element = " << myList.front();

	std::cout << "\nGet the last element = " << myList.back();

	// Remove the first element
	myList.pop_front();

	// Remove the last element
	myList.pop_back();

	// Use emplace to add an element
	myList.emplace(std::begin(myList), 125);

	// Sort the list
	myList.sort();

	std::cout << "\nList is : ";
	print(std::begin(myList), std::end(myList));
}


void howToDeque()
{
	// Create a deque of ints
	std::deque<int> gquiz;
	// Push front and back some given elements
	gquiz.push_front(23);
	gquiz.push_front(57);
	gquiz.push_front(42);
	gquiz.push_front(3);
	gquiz.push_front(93);
	std::cout << "The deque gquiz is : \n";
	for (auto it : gquiz)
	{
		std::cout << it << " ";
	}
	std::cout << "\n";

	// Remove the first elemt
	gquiz.pop_front();
	// Remove the last element
	gquiz.pop_back();

	std::cout << "The deque gquiz is :\n";
	for (auto it : gquiz)
	{
		std::cout << it << " ";
	}
	std::cout << "\n";

	// Add a new element "in place" in the deque
	gquiz.emplace(gquiz.begin() + 2, 10);
	std::cout << "The deque gquiz is :\n";
	for (auto it : gquiz)
	{
		std::cout << it << " ";
	}
	std::cout << "\n";
}

void howToArray()
{
	// Initializing the array elements 
	std::array<int, 10> myArray{ 1,2,3,4,5,6,7,8,9,10 };

	// Operations on array :-
	// 1. at() : -This function is used to access the elements of array.
	std::cout << myArray.at(5) << '\n';
	// 2. get() : -This function is also used to access the elements of array.This function is not the member of array class but overloaded function from class tuple.
	std::cout << std::get<5>(myArray) << '\n';
	// 3. operator[] :-This is similar to C - style arrays.This method is also used to access array elements.
	std::cout << myArray[5] << '\n';
	// 4. front() :-This returns the first element of array.
	std::cout << myArray.front() << '\n';
	// 5. back() : -This returns the last element of array.
	std::cout << myArray.back() << '\n';
	// 6. size() : -It returns the number of elements in array.This is a property that C - style arrays lack.
	std::cout << myArray.size() << '\n';
	// 7. max_size() : -It returns the maximum number of elements array can hold i.e, the size with which array is declared.The size() and max_size() return the same value.
	std::cout << myArray.max_size() << '\n';
	// 8. swap() : -The swap() swaps all elements of one array with other.
	std::array<int, 10> otherArray{ 1,2,3,4,5,6,132,4365,4356,10 };
	myArray.swap(otherArray);
	print(std::begin(otherArray), std::end(otherArray));
	// 9. empty() : -This function returns true when the array size is zero else returns false.
	std::cout << myArray.empty() << '\n';
	// 10. fill() :-This function is used to fill the entire array with a particular value.
	myArray.fill(5);
	print(std::begin(otherArray), std::end(otherArray));
}



void howToQueue()
{
	// Create a queue and push some elements
	std::queue<int> gquiz;
	gquiz.push(5);
	gquiz.push(27);
	gquiz.push(10);
	gquiz.push(49);
	gquiz.push(3);
	std::cout << "The queue gquiz is : ";
	printQueue(gquiz);
	gquiz.push(5);
	gquiz.push(27);
	gquiz.push(10);
	gquiz.push(49);
	gquiz.push(3);
	std::cout << "\n";
	std::cout << "\nSize : " << gquiz.size();
	std::cout << "\nFirst element: " << gquiz.front();
	std::cout << "\nLast element : " << gquiz.back();

	std::cout << "\nPop : ";
	gquiz.pop();
	std::cout << "The queue gquiz is : ";
	printQueue(gquiz);
}

void howToStack()
{
	// Create a queue and push some elements
	std::stack<int> gquiz;
	gquiz.push(5);
	gquiz.push(3);
	std::cout << "The stack gquiz is : ";
	print(gquiz);

	std::cout << "\nSize: " << gquiz.size();
	std::cout << "\nTop : " << gquiz.top();

	std::cout << "\nPop : ";
	gquiz.pop();
	std::cout << "The stack gquiz is : ";
	print(gquiz);

}
void howToSet()
{
	std::set<int, std::greater<int>> mySet;
	std::set<int>::iterator itlow, itup;

	int nrElements = 10;
	for (auto it = 1; it <= nrElements; it++)
	mySet.insert(it);

	mySet.insert(5);

	for (auto it = mySet.cbegin(); it != mySet.cend(); ++it)
		std::cout << *it << " ";
	std::cout << std::endl;

	mySet.erase(mySet.begin(), mySet.find(5));
		
	for (auto it = mySet.cbegin(); it != mySet.cend(); ++it)
		std::cout << *it << " ";
	std::cout << std::endl;

	itlow = mySet.lower_bound(2);               
	itup = mySet.upper_bound(4);

	
	std::cout << "Lower bound: " << *itlow << std::endl;
	std::cout << "Upper bound: " << *itup << std::endl;
		
	// empty int set container with greater as compare key

	// insert elements in random order

	// insert the same element twice

	// print the set

	// remove all elements up to a chosen number

	// print

	// lower bound (greater or equal )

	// upper bound (greater than value)

}

void howToMap()
{
	// empty map container
	std::map<int, float> myMap;

	// insert element in map in all methods
	myMap[2] = 3;
	myMap.insert({ 5,6 });
	myMap.insert({ 7,6 });
	// print
	for (const auto&[key, value] : myMap)
		std::cout << key << ' ' << value << '\n';
	//print(std::begin(myMap), std::end(myMap));
	// remove all elements up to element with key= ?  in myMap
	auto final = myMap.find(7);
	std::map<int, float> myOtherMap;
	for (auto it = final; it != std::end(myMap); ++it)
	{
		myOtherMap.insert({ it->first, it->second });
	}

	myMap.erase(std::begin(myMap), final);
	// print
}

void SequenceContainers()
{
	////////////////////////////
	/// Vector
	////////////////////////////
	//
	// Vectors are same as dynamic arrays with the ability to resize itself automatically when an element is inserted or deleted,
	// with their storage being handled automatically by the container. Vector elements are placed in contiguous storage so that
	// they can be accessed and traversed using iterators. In vectors, data is inserted at the end. Inserting at the end takes differential
	// time, as sometimes there may be a need of extending the array. Removing the last element takes only constant time because no resizing
	// happens. Inserting and erasing at the beginning or in the middle is linear in time.

	vectorIterators();
	vectorCapacity();
	vectorModifiers();
	vectorAccess();

	////////////////////////////
	/// List
	////////////////////////////
	//
	// Lists are sequence containers that allow non-contiguous memory allocation. As compared to vector, list has slow traversal,
	// but once a position has been found, insertion and deletion are quick. Normally, when we say a List, we talk about doubly linked list.
	// For implementing a singly linked list, we use forward list.

	howToList();

	////////////////////////////
	/// Deque
	////////////////////////////
	//
	// Double ended queues are sequence containers with the feature of expansion and contraction on both the ends. 
	// They are similar to vectors, but are more efficient in case of insertion and deletion of elements at the end, and also the beginning. 
	// Unlike vectors, contiguous storage allocation may not be guaranteed.
	// The functions for deque are same as vector, with an addition of push and pop operations for both front and backto

	howToDeque();

	////////////////////////////
	/// Array
	////////////////////////////
	//
	// The introduction of array class from C++11 has offered a better alternative for C - style arrays.
	// The advantages of array class over C - style array are :
	// - Array classes knows its own size, whereas C - style arrays lack this property.So when passing to functions, we dont need to pass size of Array as a separate parameter.
	// - With C - style array there is more risk of array being decayed into a pointer.Array classes dont decay into pointers
	// - Array classes are generally more efficient, light - weight and reliable than C - style arrays.

	howToArray();

	////////////////////////////
	/// Forward List
	////////////////////////////
	//
	// Forward list in STL implements singly linked list.Introduced from C++11, forward list are useful than other containers in insertion, 
	// removal and moving operations(like sort) and allows time constant insertion and removal of elements.
	// 
	// It differs from list by the fact that forward list keeps track of location of only next element while list keeps track to both next 
	// and previous elements, thus increasing the storage space required to store each element.The drawback of forward list is that it cannot be 
	// iterated backwards and its individual elements cannot be accessed directly.
	//
	// Forward List is preferred over list when only forward traversal is required(same as singly linked list is preferred over doubly linked list)
	// as we can save space.Some example cases are, chaining in hashing, adjacency list representation of graph, etc.
}

void ContainerAdaptors()
{
	////////////////////////////
	/// Queue 
	////////////////////////////
	//
	// Queues are a type of container adaptors which operate in a first in first out(FIFO) type of arrangement.
	// Elements are inserted at the back(end) and are deleted from the front.
	// The functions supported by queue are :
	//
	//  empty()  Returns whether the queue is empty
	//  size()  Returns the size of the queue
	//  front()  Returns a reference to the first element of the queue
	//  back()  Returns a reference to the last element of the queue
	//  push(g)  Adds the element g at the end of the queue
	//  pop()  Deletes the first element of the queue

	howToQueue();

	////////////////////////////
	/// Stack 
	////////////////////////////
	//
	// Stacks are a type of container adaptors with LIFO(Last In First Out) type of working, where a new element is added 
	// at one end and (top)an element is removed from that end only.
	//
	// The functions associated with stack are :
	//      empty()  Returns whether the stack is empty
	//      size()  Returns the size of the stack
	//      top()  Returns a reference to the top most element of the stack
	//      push(g)  Adds the element g at the top of the stack
	//      pop()  Deletes the top most element of the stack

	howToStack();
}

void AssociativeContainers()
{
	////////////////////////////
	/// Set 
	////////////////////////////
	//
	// Sets are a type of associative containers in which each element has to be unique, because the value of the element identifies it.
	// The value of the element cannot be modified once it is added to the set, though it is possible to remove and add the modified value of that element.

	howToSet();

	////////////////////////////
	/// Map 
	////////////////////////////
	//
	// Maps are associative containers that store elements in a mapped fashion.Each element has a key value and a mapped value.
	// No two mapped values can have same key values.

	howToMap();
}

void Containers()
{
	SequenceContainers();
	ContainerAdaptors();
	AssociativeContainers();
}

int increment(int x) { return (x + 1); }

void FunctorAndTransform()
{
	// init a vector of ints
	// Apply increment to all elements of arr[] and store the modified elements back in arr[] using a functor function
}

void Lambda()
{
	// Lambda syntax

	// Lambda variable

	// Lambda capture

	// Lambda argument
}

void LambdaAndAlgo()
{
	// Init a vector and push some elements

	// use for_each to multiply each element by 10

	// use for_each to print all elements


	// Init a vector

	// Resize it

	// Using fill to "fill" the vector

	// use for_each to print all elements

	//std::for_each_n(ns.begin(), 3, [](auto& n) { n *= 2; });
}

void Functors()
{
	FunctorAndTransform();

	Lambda();

	LambdaAndAlgo();
}

void Algo()
{
	//sort(first_iterator, last_iterator)  To sort the given vector.
	//reverse(first_iterator, last_iterator)  To reverse a vector.
	//max_element(first_iterator, last_iterator)  To find the maximum element of a vector.
	//min_element(first_iterator, last_iterator)  To find the minimum element of a vector.
	//accumulate(first_iterator, last_iterator, initial value of sum)  Does the summation of vector elements
	//count(first_iterator, last_iterator, x)  To count the occurrences of x in vector.
	//find(first_iterator, last_iterator, x)  Points to last address of vector((name_of_vector).end()) if element is not present in vector.
	//lower_bound(first_iterator, last_iterator, x)  returns an iterator pointing to the first element in the range[first, last) which has a value not less than x.
	//upper_bound(first_iterator, last_iterator, x)  returns an iterator pointing to the first element in the range[first, last) which has a value greater than x.
	//next_permutation(first_iterator, last_iterator)  This modified the vector to its next permutation.
	//prev_permutation(first_iterator, last_iterator)  This modified the vector to its previous permutation.
	//distance(first_iterator,desired_position)
}

int main()
{
	Containers();

	//Functors();

	//Algo();


	std::cin.get();
	return 0;
}

/*

Echipa (R)CPC
SET:
1.std::emplace_hint - e ca emplace dar iti da hint (unde incepi cautarea)
1. std::less - comparatorul unui set

Array:
3. array.swap(altArray)
4. ConstantIterator - cbegin - iteratorul e constant.

Echipa Team : reluu Rectified Linear Unit
- Ξn cazul ξn care vrem sa ?tergem ultimul element/golim vectorul folosim mai bine pentru eficien?a .pop_back() ξn loc de .erase();
- Usor de folosit(name of functions speak for theirself)
- Erase este destul de inselator
- for seach nu merge din ceva motive
- ::insert_or_assign() pentru a suprascrie valoarea pe cheie


Echipa BBDN:
1. Lista are .sort() in loc de std::sort
2. Queue si stack nu au iteratori.
3. Erase sterge elementul de la o pozitie si remove un element.
4. Putem seta comparatorul la containerele asociative.
5. lower & upper bound


Echipa IML:
-utilizarea iteratorilor si diferentele dintre ei
-daca ai nevoie sa iterezi peste un queue sau stack, atunci nu ai nevoie de ele

set & map:
-au o parte din interfata asemanatoare: erase (exact aceeasi parameteri), insert

array:
-swap() ma gandeam ca interschimba elemente
-std::get<N> overload

Echipa IMF:
//Procedura de printare a cozii
//std get
//Afisarea mapei
//Invalidarea iteratorilor
//Setul nu poate contine duplicate
//Iteratorii sunt niste pointeri
*/